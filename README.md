akisys.disk-layout
==================

Managing LVM-based disk layouts

Requirements
------------

None

Role Variables
--------------

* `disk_layout_managed_enabled: false`  
  bool switch for role enablement

* `disk_layout_managed_vgs: []`  
  list of to-be-managed LVM volume groups

* `disk_layout_vgs_layout: {}`  
  dict of to-be-managed LVM volumes per volume group

Dependencies
------------

None

Example Playbook
----------------

* See [Unencrypted partitions](molecule/default/plain-playbook.yml)
* See [Encrypted partitions](molecule/default/crypt-playbook.yml)

Testing
-------

> Don't use the `default` scenario, it's only a place for common definitions
> here.

* `molecule test -s single-partition`
* `molecule test -s single-crypt-parition`
* `molecule test -s trailing-partition`
* `molecule test -s trailing-crypt-parition`

License
-------

MIT

Author Information
------------------

Alexander Kuemmel
